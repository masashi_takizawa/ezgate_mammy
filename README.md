# gulp_template
**Welcome to the gulp_template wiki!**

**gulp** + **bower** で開発環境を整えます。
_**CoffeeScript**_ と _**Compass**_ を使用します。

### まず開発に必要な環境を整えます。すでに整っていたら無視してください。<br>
※node.jsの環境構築は省きます。
> * **ruby**をインストールします。(macは元から入っています。)<br>
> [http://rubyinstaller.org/downloads/](http://rubyinstaller.org/downloads/)

> * **sass**をインストールします。<br>
> `gem install sass`

> * **compass**をインストールします。<br>
> `gem update --system`  (念の為)<br>
> `gem install compass`

> * compassのプラグイン_**rgbapng**_をインストールします。<br> 
>  `gem install compass-rgbapng`

> * **bower** をインストールします。<br>
> `npm install -g bower`

> * **gulp_template**をcloneします。<br>
> `git clone https://github.com/dig-taki/gulp_template.git ディレクトリ名`


<br>
### cloneしたフォルダ内で初期化
> 1. nodeモジュールをインストール <br>
>  `npm install`

> 2. ライブラリーをインストール(初期設定ではbootstrap:jquery含む,html5shiv.js,selectivizr.js,css3-mediaqueries-js)<br>
> `bower install`

> 3. 初期化<br>
> `gulp`

>以上です。

<br>
####予備知識
> * bowerを使用して他のライブラリをインストール<br>
> ここであるか調べてから。(無くてもgitなどのURLでも入れられます)<br>
> [http://bower.io/search/](http://bower.io/search/)<br>
> `bower install ライブラリ名 --save` 

> * 他のnodeモジュールをインストール<br>
> `npm install coffee-script --save-dev`
