$(function () {
    
    //スマホ電話番号リンク付与(span.tel-link)
    var ua = navigator.userAgent;
    if (ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1) {
        $('.tel-link').each(function() {
            var $img = $(this).find("img");
            var str = ($img.length) ? $img.attr("alt") : $(this).text();
            $(this).wrap("<a href='tel:" + str.replace(/-/g, '') + "'></a>");
            //$(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        });
    }
    //ページ内スムーススクロール
    $('a[href^=#]'+'.scroll-link').click(function () {
        // スクロールの速度
        var speed = 400; // ミリ秒
        // アンカーの値取得
        var href = $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({ scrollTop: position }, speed, 'swing');
        return false;
    });

    //横並びの高さ揃える
    $('.match').matchHeight();

    $('.btn-faq').on("click",function(e){
        $(this).toggleClass("open");
        var $content = $(this).parents(".section-faq").find('.content-faq');
        $content.slideToggle();

    })

    $('.thumbs-gallery a').click(function() {
        $(this).parents("ul").find("li").removeClass("active");
        $(this).parents("li").addClass("active");
        var imgSrc = $(this).attr("href");
        var text = $(this).data("text");
        var $mainImg = $(this).parents(".gallery-wrapp").find(".img-gallery-main .img-main");
        var $text = $(this).parents(".gallery-wrapp").find(".text-main");
        $text.html(text);
        $mainImg.css('background-image', 'url('+ imgSrc + ')');
        $mainImg.hide();
        $mainImg.show();
        return false;
    });

    $('.details-service').hide();
    $('.btn-service').click(function() {
        $(this).parents(".content").find(".details-service").slideToggle();
    })
});
