//ArrayオブジェクトにindexOf追加
if(!Array.prototype.indexOf){
	Array.prototype.indexOf = function(target,index){
		if(isNaN(index)){
			index = 0;
		}
		for(var i = index; i < target.length; i++){
			if(this[i] === target){
				return i;
			}
		}
		return -1;
	};
}

//ArrayオブジェクトにforEach追加
if ( !Array.prototype.forEach ) {
	Array.prototype.forEach = function( callback, thisArg ) {
		var T, k;
		if ( this == null ) {
			throw new TypeError( " this is null or not defined" );
		}
    var O = Object(this);
    var len = O.length >>> 0; // Hack to convert O.length to a UInt32
    if ( {}.toString.call(callback) != "[object Function]" ) {
    	throw new TypeError( callback + " is not a function" );
    }
    if ( thisArg ) {
    	T = thisArg;
    }
    k = 0;
    while( k < len ) {
    	var kValue;
      if ( k in O ) {
        kValue = O[ k ];
        callback.call( T, kValue, k, O );
    }
      k++;
  }
};
}